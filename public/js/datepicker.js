(function(global) {
    function getTime(elem) {
        const date = $(elem).datepicker('getDate');
        return date ? date.getTime() : null;
    }

    const UNITS = [60, 60, 24, 30, 12].reduce((arr, factor) => {
        arr.push(factor * arr[arr.length - 1]);
        return arr;
    }, [1]), UNIT_STRING = ['segundos', 'minutos', 'horas', 'días', 'meses', 'años'];

    function initUI() {
        const $fields = $('#date1, #date2');
        $fields.datepicker().change(function(e) {
            const [date1, date2] = Array.prototype.map.call($fields, getTime);
            if (date1 != null && date2 != null) {
                let seconds = Math.abs(date1 - date2) / 1000;
                $('#diff-text').text(UNITS.reduceRight((arr, unit, index) => {
                    const div = (seconds - (seconds %= unit)) / unit;
                    if (div > 0)
                        arr.push(`${div} ${UNIT_STRING[index]}`);

                    return arr;
                }, []).join(', '));
            }
        });
    }

    document.addEventListener('DOMContentLoaded', initUI);
})(this);
