(function(global) {
    function initUI() {
        $( "#pending-tasks, #fulfilled-tasks").sortable({
            containment: '#main-container',
            connectWith: '.sortable',
            revert: true
        });

        $('#pending-tasks').on('sortupdate', function(e, ui) {
            const item = ui.item.get(0);
            if (item.gotAdded) {
                item.className = 'ui-sortable-handle';
                item.removeAttribute('style');

                const input = item.querySelector('input');
                input.addEventListener('blur', function(e) {
                    if (this.value == '') {
                        let indices = 0, matcher = /^Tarea (\d+)$/;
                        for (const item of document.querySelectorAll('.ui-sortable-handle')) {
                            let index = item.textContent.match(matcher);
                            if (index == null)
                                continue

                            index = Number(index[1]);
                            indices |= (1 << index);
                        }

                        for (var i = 1; indices & (1 << i); i++);
                        this.value = `Tarea ${i}`;
                    }

                    item.textContent = this.value;
                    this.remove();
                });

                input.addEventListener('keyup', function(e) {
                    if (e.keyCode == 13)
                        this.blur();
                });

                input.focus();
                delete item.gotAdded;
            }

            $(this).siblings('.tasklist-state').toggle(this.children.length == 0);
        });

        $('.draggable').draggable({
            connectToSortable: "#pending-tasks",
            helper: function(e) {
                const item = document.createElement('li');
                const input = document.createElement('input');
                input.classList.add('task-input')
                item.classList.add('ui-sortable-handle');
                item.appendChild(input);
                return item;
            },

            revert: "invalid",
            start: function(e, ui) {
                ui.helper.css('width', this.offsetWidth);
            },

            stop: function(e, ui) {
                const helper = ui.helper.get(0);
                if (helper.parentElement != this)
                    helper.gotAdded = true;
            }
        });
    }

    document.addEventListener('DOMContentLoaded', initUI);
})(this);
